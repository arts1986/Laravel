# Laravel

#### 项目介绍
Laravel是一套简洁、优雅的PHP Web开发框架(PHP Web Framework)。它可以让你从面条一样杂乱的代码中解脱出来；它可以帮你构建一个完美的网络APP，而且每行代码都可以简洁、富于表达力。

<h3>Laravel安装包</h3>
<p><a href="http://download.laravelacademy.org/laravel57.zip">Laravel 5.7 一键安装包</a></p>
<p><a href="http://download.laravelacademy.org/laravel56.zip">Laravel 5.6 一键安装包</a>
</p>
<p>
<a href="http://download.laravelacademy.org/laravel55.zip">Laravel 5.5 一键安装包</a>
</p>
<p>
<a href="http://download.laravelacademy.org/laravel54.zip">Laravel 5.4 一键安装包</a>
</p>
<p>
<a href="http://download.laravelacademy.org/laravel-5_3.zip" target="_blank" rel="noopener">Laravel 5.3 一键安装包</a>
</p>
<p>
<a href="http://download.laravelacademy.org/laravel52.zip" target="_blank" rel="noopener">Laravel 5.2 一键安装包</a>
</p>
<p>
<a href="http://download.laravelacademy.org/laravel51.zip" target="_blank" rel="noopener">Laravel 5.1 一键安装包</a></p>